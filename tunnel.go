package sshtunnel

import (
	"golang.org/x/crypto/ssh"
	"net"
	"io"
	"fmt"
	"strconv"
	"log"
)

type HostPort struct {
	Host string
	Port uint16
}

func (h *HostPort) String() string {
	return fmt.Sprintf("%s:%d", h.Host, h.Port)
}

type SSHTunnel struct {
	LocalPort uint16
	Remote HostPort
	Gateway HostPort
	Config *ssh.ClientConfig
	Logger *log.Logger
}

func (t *SSHTunnel) Tunnel(c chan error) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", t.LocalPort))
	if err != nil {
		c <- err
		if t.Logger != nil {
			t.Logger.Println("Tunnel: %s", err)
		}
		return
	}
	defer listener.Close()
	_, sport, err := net.SplitHostPort(listener.Addr().String())
	if err != nil {
		c <- err
		if t.Logger != nil {
			t.Logger.Println("Tunnel: %s", err)
		}
		return
	}
	iport, err := strconv.Atoi(sport)
	if err != nil {
		c <- err
		if t.Logger != nil {
			t.Logger.Println("Tunnel: %s", err)
		}
		return
	}
	t.LocalPort = uint16(iport)

	for {
		conn, err := listener.Accept()
		if err != nil {
			c <- err
			if t.Logger != nil {
				t.Logger.Println("Tunnel: %s", err)
			}
			return
		}
		go t.forward(conn, c)
	}

}

func (t *SSHTunnel) forward(localConn net.Conn, c chan error) {
	serverConn, err := ssh.Dial("tcp", t.Gateway.String(), t.Config)
	if err != nil {
		c <- err
		if t.Logger != nil {
			t.Logger.Println("foward: %s", err)
		}
		return
	}

	remoteConn, err := serverConn.Dial("tcp", t.Remote.String())
	if err != nil {
		if t.Logger != nil {
			t.Logger.Println("foward: %s", err)
		}
		c <- err
		return
	}

	// Currently, we're just swallowing errors since this should be only called from a goroutine.
	// We may want to consider adding a logger to the tunnel struct
	copyConn := func(writer, reader net.Conn) {
		_, err := io.Copy(writer, reader)
		if err != nil && t.Logger != nil {
			t.Logger.Println("foward: copyConn: %s", err)
		}
	}

	go copyConn(localConn, remoteConn)
	go copyConn(remoteConn, localConn)
}